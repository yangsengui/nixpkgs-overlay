{ pkgs ? import <nixpkgs> {} }:
let
    code-server-build = version: pkgs.stdenv.mkDerivation rec {
        name = "code-server";

        src = builtins.fetchTarball {
            url = "https://github.com/coder/code-server/releases/download/v${version}/code-server-${version}-linux-amd64.tar.gz";
        };
        phases = ["buildPhase"];
        buildPhase = ''
            cp -r $src/ $out/
        '';
    };

in
    {
        v4-22-1 = code-server-build "4.22.1";
    }
