{ pkgs }:
let
    myPoetry = pkgs.stdenv.mkDerivation {
        name = "pycharm-professional";
        version = "2023.1.1";

        src = builtins.fetchTarball {
          url = "https://download.jetbrains.com/python/pycharm-professional-professional-2023.1.1.tar.gz";
        };
        phases = ["buildPhase"];
        buildPhase = ''
            cp -r $src/ $out/
        '';
      };
in
    myPoetry
